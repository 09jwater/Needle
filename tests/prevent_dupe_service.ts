import 'jest';
import {provide} from ".."

describe('the injection manager', () => {

    class FooService {

        name : string;

    }

    it(`should not reconstruct the same service twice`, () => {
        let fooService1 = provide(FooService);
        let fooService2 = provide(FooService);

        fooService1.name = "hello";
        expect(fooService2.name == "hello");
    });

});