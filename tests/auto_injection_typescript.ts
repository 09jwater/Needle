import 'jest';
import {autoInject, create} from ".."

describe('this class', () => {

    class FooService {

        double(value: number) {
            return 2 * value;
        }

    }

    class BarService {

        half(value: number) {
            return 0.5 * value;
        }

    }

    @autoInject
    class Object {

        constructor(public fooService: FooService, private barService: BarService) {

        }

        half(value: number) {
            return this.barService.half(value);
        }

    }

    it(`should have all params of its constructor injected`, () => {

        let obj = create(Object);
        expect(obj.fooService.double(2) == 4 && obj.half(12) == 6);

    });

});