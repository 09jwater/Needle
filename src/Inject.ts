import InjectionManager from "./InjectionManager";
import "reflect-metadata"

export default function inject<T>(target: any, key: string): any {
    return InjectionManager.instance().defineInjectedProperty(target, key);

}