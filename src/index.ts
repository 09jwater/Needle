import inject from "./Inject";
import autoInject from "./AutoInject";

import {create} from "./Create";

import InjectionManager from "./InjectionManager";
import {Class} from "./util/Class";

const injectionManagerInstance = InjectionManager.instance();

const use: ((clazz: Class<any> | Array<Class<any>>) => void) = injectionManagerInstance.use.bind(injectionManagerInstance);
const provide: (<T>(clazz: Class<T>) => T) = injectionManagerInstance.provide.bind(injectionManagerInstance);

export {
    inject,
    autoInject,
    create,
    InjectionManager,
    use,
    provide,
};


