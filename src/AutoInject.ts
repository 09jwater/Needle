import "reflect-metadata"
import InjectionManager from "./InjectionManager";

function getParamNames(fn: any): string[] {
    let reComments = /\/\*[\s\S]*?\*\/|\/\/.*?[\r\n]/g;
    let reParams = /\(([\s\S]*?)\)/;
    let reNames = /[$\w]+/g;
    return ((fn.toString().replace(reComments, '').match(reParams) || [0, ''])[1] as any).match(reNames) || [];
}

export default function autoInject<T>(target: any): any {

    let paramNames = getParamNames(target);
    let paramTypes = Reflect.getMetadata("design:paramtypes", target);
    if (!paramTypes) {
        InjectionManager.instance().throwReflectionError(target, "constructor");
    }

    let original = target;

    let injectedClass = class extends original {
        constructor(...args : any[]) {
            super(args);

            for (let i = 0 ; i < paramNames.length; i++) {
                InjectionManager.instance().defineInjectedProperty(this, paramNames[i], paramTypes[i]);
            }
        }
    };

    injectedClass.prototype = original.prototype;
    Object.defineProperty(injectedClass, 'name', { writable: true });
    injectedClass.name = original.name;

    return injectedClass;
}

