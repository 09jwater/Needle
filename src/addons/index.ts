import withNeedle from "./next/withNeedle";

const next = {
    withNeedle,
};

export {
    next,
}