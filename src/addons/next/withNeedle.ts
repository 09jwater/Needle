import * as path from "path";
import * as fs from "fs";

export default (nextConfig: any = {}) => {
    if (!nextConfig.pageExtensions) {
        nextConfig.pageExtensions = ['jsx', 'js']
    }

    if (nextConfig.pageExtensions.indexOf('ts') === -1) {
        nextConfig.pageExtensions.unshift('ts')
    }

    if (nextConfig.pageExtensions.indexOf('tsx') === -1) {
        nextConfig.pageExtensions.unshift('tsx')
    }

    console.log("> Adding typescript support");

    let tsConfigPath = path.resolve('./tsconfig.json');
    let tsConfig = JSON.parse(fs.readFileSync(tsConfigPath).toString());
    let {compilerOptions} = tsConfig;

    if (!compilerOptions || !compilerOptions.emitDecoratorMetadata || !compilerOptions.experimentalDecorators) {
        console.error(`Typescript config at '${tsConfigPath}' does not have 'emitDecoratorMetadata' and 'experimentalDecorators' both set to true. This is required for Needle to fully function.`);
    }

    return {
        ...nextConfig,
        ...{
            webpack: (config: any, options: any) => {
                let {rules} = config.module;

                config.resolve.extensions.push('.ts', '.tsx');
                config.module.rules = rules.map((rule: any) => {
                    let {test, use} = rule;
                    test = test.toString();
                    let isJs = test === '/\\.(js|jsx)$/';
                    if (isJs) {
                        if (use) {
                            rule.use = [use, {
                                loader: 'ts-loader', options: {
                                    configFile: tsConfigPath
                                }
                            }];
                        }
                        rule.test = /\.(js|jsx|ts|tsx)$/;
                    }
                    return rule;
                });

                if (typeof nextConfig.webpack === 'function') {
                    return nextConfig.webpack(config, options)
                }

                return config;
            }
        }
    }
};