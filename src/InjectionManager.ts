import {Class} from "./util/Class";

export interface InjectionMapping<T> {
    clazz: Class<T>;
    value: T;
}

export default class InjectionManager {

    private static currentInstance: InjectionManager;

    static instance(): InjectionManager {
        return this.currentInstance ? this.currentInstance : this.currentInstance = new InjectionManager();
    }

    private mappings: Array<InjectionMapping<any>> = [];

    use(clazz: Class<any> | Array<Class<any>>) {
        if (Array.isArray(clazz)) {
            clazz.map(this.use.bind(this));
        } else {
            this.setMapping({clazz, value: new (clazz as any)()});
        }
    }

    throwReflectionError(target : any, key : string) {
        let targetName = target.constructor && target.constructor.name;
        throw new Error(`No reflection data for '${key}' in ${targetName ? `'${targetName}'` : 'target'}. Do you have 'emitDecoratorMetadata' set to true in tsconfig?`);
    }

    defineInjectedProperty(target : any, key : string, type : any = null) {
        if (!type) {
            type = Reflect.getMetadata("design:type", target, key);
        }

        return Object.defineProperty(target, key, {
            get: () => {
                if (!type) {
                    this.throwReflectionError(target, key);
                }
                return this.provide(type);
            },
            set: () => {
                throw new Error("Cannot set inject property");
            },
            enumerable: true,
            configurable: true,
        });
    }

    provide<T>(clazz: Class<T>): T {
        return this.getMapping(clazz).value;
    }

    private setMapping<T>(mapping: InjectionMapping<T>) {
        this.mappings.push(mapping);
    }

    getMapping<T>(clazz: Class<T>): InjectionMapping<T> {
        if (!clazz) {
            throw new Error("Cannot get mapping of null/undefined class");
        }

        let mapping: InjectionMapping<any> | null = null;
        for (let injectable of this.mappings) {
            if (injectable.value instanceof clazz) {
                mapping = injectable;
                break;
            }
        }
        if (mapping) {
            return mapping;
        }

        mapping = {clazz, value: new (clazz as any)()};
        this.setMapping(mapping);
        return mapping;
    }

}
