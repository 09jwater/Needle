import {Class} from "./util/Class";

export function create<T>(clazz: Class<T>): T {
    return new (clazz as any);
}